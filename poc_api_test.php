<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 28-12-2018
 * Time: 19:43
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>POC API test</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>


<?php
if(isset($_GET["action"]) && isset($_GET["value"])){
    $action = $_GET["action"];
    $value = $_GET["value"];
}
else{
    echo "Please fill in action and value GET parameters";
    exit();
}

?>
    <script>

        // Make an AJAX call to the file
        $.get( 'https://sandervanderburgt.com/kingsen_poc/api/?type=lobby&action=<?php echo $action ?>&value=<?php echo $value?>', function( data ) {
        })
            .done(function(data) {
                // console log the returned data
                console.log(data)
            })
            .fail(function() {
                // if something goes wrong, write it in the console
                console.log("Something went wrong!");
            })
    </script>

</body>
</html>


