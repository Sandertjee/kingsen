<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form>
    <input type="text" id="CreateLobbyUsername" placeholder="Name"/>
    <input type="button" id="createlobby" value="Create Lobby"/>
</form>

<hr/>
<form>
    <h2>Join game:</h2>
    <input type="text" id="Lobbycode" placeholder="Lobby code"/>
    <input type="text" id="Username" placeholder="Username"/>
    <input type="button" value="Join game">
</form>




<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<?php
session_start();
?>
    <script>

        $("#createlobby").on("click", function(){
            if($("#CreateLobbyUsername").val() === ""){
                alert("please fill in all fields!")
            }
            else{
                generateroomcode();
            }
        });


        function generateroomcode(){
            var possibilities = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var roomcode = "";

            for(var i = 0; i < 5; i++){
                roomcode += possibilities.charAt(Math.floor(Math.random() * possibilities.length));
            }

            if(roomexists(roomcode)){
                generateroomcode();
            }
            else{

                // create lobby using API
                $.get("https://sandervanderburgt.com/kingsen_poc/api/?type=lobby&action=new&value=" + roomcode)
                    .done(function() {
                        // Exists
                        console.log("Room created");
                        // set session to roomcode for future reference
                        sessionStorage.setItem('roomcode', roomcode);
                        return true;
                    }).fail(function() {
                        // Doesn't exist
                        console.log("I want to die");
                        return false;
                });

                // Testing load from sessiondata
                var data = sessionStorage.getItem('roomcode');
                // display roomcode for test
                console.log(data);

                // generate new player when room is created
                $.get("https://sandervanderburgt.com/kingsen_poc/api/?type=player&action=new&value=" + roomcode)
                    .done(function() {
                        // Exists
                        console.log("Room created");
                        sessionStorage.setItem('roomcode', roomcode);
                        return true;
                    }).fail(function() {
                    // Doesn't exist
                    console.log("I want to die");
                    return false;
                });



            }

        }


        function roomexists(roomcode){

            $.get("https://sandervanderburgt.com/kingsen_poc/rooms/" + roomcode)
                .done(function() {
                    // Exists
                    console.log("Room Already Exists!");
                    return true;
                }).fail(function() {
                    // Doesn't exist
                    console.log("Room Doesn't exist");
                    return false;
            });
            return false;
        }


    </script>

</body>
</html>


