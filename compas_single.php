<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>



<h1 id="counter_north"> north</h1>

<h2>Point near north, please</h2>
<p>North: 330 - 30</p>
<p>East: 60 - 120</p>
<p>South: 150 - 210</p>
<p>West: 240 - 300</p>


<p id="n"></p>
<p id="u"></p>
<p id="l"></p>

<script>

    var counter_north = 0;

    var start_counter_north = false;

    var northinterval;




    window.addEventListener('deviceorientation', function(event) {


        var beta = event.beta;
        var gamma = event.gamma;

        if(event.webkitCompassHeading){
            alpha = event.webkitCompassHeading;
        }


        // North
        if(alpha >= 330 && start_counter_north === false){
            start_counter_north = true;
            startcountingnorth(true);
        }
        else if(alpha > 330 && start_counter_north === true){

        }
        else{
            startcountingnorth(false);
            start_counter_north = false;
        }

        document.getElementById("n").innerHTML = alpha;
        document.getElementById("u").innerHTML = beta;
        document.getElementById("l").innerHTML = gamma;
    });


    function startcountingnorth(value){
        if(value === true){
            northinterval = setInterval(function(){
                counter_north++;
                document.getElementById("counter_north").innerHTML = counter_north;
            }, 1000);
        }
        else{
            clearInterval(northinterval);
        }
    }







</script>
</body>
</html>