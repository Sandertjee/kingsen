<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>



<h1 id="counter_north"> north</h1>
<h1 id="counter_east"> Est</h1>
<h1 id="counter_south"> south</h1>
<h1 id="counter_west"> west</h1>

<h2>Point anywhere please</h2>
<p>North: 330 - 30</p>
<p>East: 60 - 120</p>
<p>South: 150 - 210</p>
<p>West: 240 - 300</p>


<p id="n">North: </p>
<p id="u">North: </p>
<p id="l">North: </p>

<script>

    var counter_north = 0;
    var counter_east = 0;
    var counter_west = 0;
    var counter_south = 0;


    var start_counter_north = false;
    var start_counter_east = false;
    var start_counter_west = false;
    var start_counter_south = false;

    var northinterval;
    var eastinterval;
    var southinterval;
    var westinterval;

    var pointto = "North";



    window.addEventListener('deviceorientation', function(event) {
        var alpha;
        var beta = event.beta;
        var gamma = event.gamma;

        if(event.webkitCompassHeading){
            alpha = event.webkitCompassHeading;
        }


        // North
        if(alpha >= 330 || alpha <= 30 && start_counter_north === false){
            start_counter_north = true;
            startcountingnorth(true);
        }
        else if(alpha >= 330 || alpha <= 30 && start_counter_north === true){
            if(pointto === "North" && counter_north >= 5){
                stopevent();
            }
        }
        else{
            startcountingnorth(false);
            start_counter_north = false;
        }


        //East
        if(alpha >= 60 && alpha <= 120 && start_counter_east === false){
            start_counter_east = true;
            startcountingeast(true);
        }
        else if(alpha > 60 && alpha <= 120 && start_counter_east === true){
            if(pointto === "East" && counter_east >= 5){
                stopevent();
            }
        }
        else{
            startcountingeast(false);
            start_counter_east = false;
        }


        //South
        if(alpha >= 150 && alpha <= 210 && start_counter_south === false){
            start_counter_south = true;
            startcountingsouth(true);
        }
        else if(alpha > 150 && alpha <= 210 && start_counter_south === true){
            if(pointto === "South" && counter_south >= 5){
                stopevent();
            }
        }
        else{
            startcountingsouth(false);
            start_counter_south = false;
        }

        //West
        if(alpha >= 240 && alpha <= 300 && start_counter_west === false){
            start_counter_west = true;
            startcountingwest(true);
        }
        else if(alpha > 240 && alpha <= 300 && start_counter_west === true){
            if(pointto === "West" && counter_west >= 5){
                stopevent();
            }
        }
        else{
            startcountingwest(false);
            start_counter_west = false;
        }



        document.getElementById("n").innerHTML = alpha;
        document.getElementById("u").innerHTML = beta;
        document.getElementById("l").innerHTML = gamma;
    });


    function startcountingnorth(value){
        if(value === true){
            northinterval = setInterval(function(){
                counter_north++;
                document.getElementById("counter_north").innerHTML = counter_north;
            }, 1000);
        }
        else{
            clearInterval(northinterval);
        }
    }

    function startcountingeast(value){
        if(value === true){
            eastinterval = setInterval(function(){
                counter_east++;
                document.getElementById("counter_east").innerHTML = counter_east;
            }, 1000);
        }
        else{
            clearInterval(eastinterval);
        }
    }

    function startcountingsouth(value){
        if(value === true){
            southinterval = setInterval(function(){
                counter_south++;
                document.getElementById("counter_south").innerHTML = counter_south;
            }, 1000);
        }
        else{
            clearInterval(southinterval);
        }
    }

    function startcountingwest(value){
        if(value === true){
            westinterval = setInterval(function(){
                counter_west++;
                document.getElementById("counter_west").innerHTML = counter_west;
            }, 1000);
        }
        else{
            clearInterval(westinterval);
        }
    }

    function stopevent(){
        counter_north = 0;
        counter_east = 0;
        counter_west = 0;
        counter_south = 0;
    }


</script>
</body>
</html>