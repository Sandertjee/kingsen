<?php

// include all classes
include_once "classes/Lobby.php";
include_once "classes/FileEditor.php";

// set $type if the getter type is set, else return NULL
$type = (isset($_GET["type"]))? $_GET["type"]:NULL;
// set $action if the getter action is set, else return NULL
$action = (isset($_GET["action"]))? $_GET["action"]:NULL;
// set $value if the getter value is set, else return NULL
$value = (isset($_GET["value"]))? $_GET["value"]:"";
// Explode $value by comma, we do this so multiple values can be given
$value = explode(",", $value);

// set variables to a NULL in string if the value is NULL, prevents switch from dying
if($type == NULL || $action == NULL || empty($value)){
    $type = "NULL";
    $action = "NULL";
    $value = "NULL";
}

// switch what type of API call it is
switch($type){
    case "lobby":
        $lobby = new Lobby($action, $value);
        break;

    default:
        // Echo something if people try to get to the page with not the correct information
        echo "<h1>No</h1>";
        break;
}