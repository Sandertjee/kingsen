<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 12-12-2018
 * Time: 12:35
 */

class FileEditor {

    // define public variables
    public $path;

    // Called when creating class, gives path we want to put stuff in
    public function __construct($path) {
        //make public path the path we get from the constructor
        $this->path = $path;
    }
    
    // Our function to create a file, needs a name, an extension and contents
    public function createfile($name, $extension, $contents){
        // opens file and prepares name, extension, if no file there, creates new one. 
        $newfile = fopen($this->path.$name.".".$extension, 'w');
        // writes contents to the newly created file
        fwrite($newfile, $contents);
        // closes the file
        fclose($newfile);
    }

    public function createfolder($name){
        // create folder with the name that is given through the method
        mkdir($this->path.$name);
        // set the current path to the new path of the folder we just created
        $this->path = $this->path.$name."/";

    }

    public function getfile($name, $extension){
        // use try catch for the file doesn't exist
        try{
            // Get the file contects using filename and extension and put it in the variable
            $file = file_get_contents($this->path.$name.".".$extension);
            // if the file exists
            if($file){
                // return the contents of the file
                return $file;
            }
            else{
                return "File doesn't exist";
            }
        }
        // catch exception
        catch(Exception $e){
            // return the exception
            return $e;
        }

    }

    public function savefile($name, $extension, $contents){
        if(file_exists($this->path.$name.".".$extension)) {

            // opens file and prepares name, extension
            $editedfile = fopen($this->path . $name . "." . $extension, 'w');
            // writes contents to the newly created file
            fwrite($editedfile, $contents);
            // closes the file
            fclose($editedfile);
            return $contents;
        }
        else{
            return "File doesn't exist";
        }
    }

    public function deletefile($name, $extension){
        // if the file is deleted safly
        if(unlink($this->path.$name.".".$extension)){
            return true;
        }
        else{
            return false;
        }

    }

    public function deletefolder($name){

    }

}