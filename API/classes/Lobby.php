<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 12-12-2018
 * Time: 12:38
 */

// define class
class Lobby {
    // when the class is called, this function is executed
    public function __construct($action, $value) {

        // call new Class fileEditor with the path we want
        $fileeditor = new FileEditor(__DIR__ . "/../../rooms/");

        // switch the action that needs to happen
        switch($action){
            // test case
            case "test":
                // give a result back
                echo "Dit is een php echo die uitgevoerd wordt in de lobby.php ".$value[0];
                break;

                // case for creating file
            case "createfile":
                // call to method createfile in fileEditor class. Gives the name and extension we want for the file to the method.
                $fileeditor->createfile($value[0], $value[1], "{}");
                // Console log the result
                echo "file ".$value[0].".".$value[1]." created";
                break;

            case "createfolder":
                // call to method createfolder in fileEditor Gives the name we want for the folder to the method.
                $fileeditor->createfolder($value[0]);
                // return a message about the folder that has been created
                echo "folder ".$value[0]." has been created";
                break;

            case "getcontent":
                // return whatever getfile() returns
                echo $fileeditor->getfile($value[0], $value[1]);
                break;

            case "savefile":
                // return whatever savefile() returns
                echo $fileeditor->savefile($value[0], $value[1], $value[2]);
                break;

            case "deletefile":
                // if the method deletefile returns true
                if($fileeditor->deletefile($value[0], $value[1])){
                    // return that the file was safely deleted.
                    echo "File ".$value[0].".".$value[1]." safely deleted";
                }
                else{
                    // return that something went wrong deleting the file
                    echo "Something went wrong deleting file ".$value[0].".".$value[1];
                }
                break;

                // Default result
            default:
                echo "Unknown action";
                break;
        }
    }
}